<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $authors = [
            [
                'display_name' => 'Ben Silbermann',
                'name' => '8en',
                'description' => 'Co-Founder and CEO @Pinterest',
            ],
            [
                'display_name' => 'Kent Bennett',
                'name' => 'kentbennett',
                'description' => 'husband, venture capitalist @bessemervp, former greenskeeper',
            ],
            [
                'display_name' => 'Byron Deeter',
                'name' => 'bdeeter',
                'description' => 'Partner @bessemerv w/@twilio @servicetitan @hashicorp @intercom @guildeducation @procoretech @ncino @scalefactor @gainsighthq @canva @docusign',
            ],
            [
                'display_name' => 'Talia Goldberg',
                'name' => 'TaliaGold',
                'description' => 'SF by day, NYC by blood, PDX at heart. Partner @BessemerVP',
            ],
        ];
        foreach ($authors as $author) {
            DB::table('authors')->insert($author);
        }
    }
}
