@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
{{--                <div class="card-header">Home</div>--}}
                <div class="card-body">
                    @foreach($messages as $k => $message)
                    @if ($k > 0)
                        <hr>
                    @endif
                    <div class="media">
                        <div class="media-body">
                            <h5 class="mt-0">{{$message->author->display_name}} <span style="color: darkgray;font-size: 0.9rem">{{$message->posted_at}}</span></h5>
                            <p style="margin-bottom: 0rem">{!! preg_replace('#((https?|ftp)://(\S*?\.\S*?))([\s)\[\]{},;"\':<]|\.\s|$)#i',"<a href=\"$1\" target=\"_blank\">$3</a>$4",$message->text) !!}</p>
                                @if($message->media_type =='photo')
                                <p>
                                    <img style="max-width: 100%;max-height: 20rem" src="{{$message->media_url}}">
                                </p>
                                @endif
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
