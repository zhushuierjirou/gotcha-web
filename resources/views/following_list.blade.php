@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
{{--                <div class="card-header">Home</div>--}}
                <div class="card-body">
                    <form class="form-inline" method="post" action="{{route('add_following')}}">
                        <div class="form-group">
                            <label class="mr-2" for="twitter_id">Twitter ID</label>
                            <input type="text" class="form-control mr-2" id="twitter_id" name="twitter_id">
                        </div>
                        <button type="submit" class="btn btn-outline-primary">Follow</button>
                        @csrf
                    </form>
                    @foreach($authors as $k => $author)
                    <hr>
                    <div class="media">
                        <div class="media-body">
                            <h5 class="mt-0">
                                {{$author->display_name}} <span style="color: darkgray;font-size: 0.9rem">{{"@" . $author->name}}</span>
                                <button type="submit" class="btn btn-sm btn-outline-danger" style="float: right"
                                        form="form-author-{{$author->id}}">
                                    Unfollow
                                </button>
                                <button type="submit" class="btn btn-sm btn-outline-success mr-2" style="float: right"
                                        form="form-sync-author-{{$author->id}}">
                                    Sync
                                </button>
                                <form id="form-sync-author-{{$author->id}}" method="post" action="{{route('sync_following')}}">
                                    <input type="hidden" name="author_id" value="{{$author->id}}">
                                    @csrf
                                </form>
                                <form id="form-author-{{$author->id}}" method="post" action="{{route('delete_following')}}">
                                    <input type="hidden" name="author_id" value="{{$author->id}}">
                                    @csrf
                                </form>
                            </h5>
                            <p style="margin-bottom: 0rem">{{$author->description}}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
