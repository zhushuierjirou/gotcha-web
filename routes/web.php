<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/following_list', 'HomeController@following_list')->name('following_list');
Route::post('/add_following', 'HomeController@add_following')->name('add_following');
Route::post('/delete_following', 'HomeController@delete_following')->name('delete_following');
Route::post('/sync_following', 'HomeController@sync_following')->name('sync_following');
