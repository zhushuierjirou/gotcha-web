<?php

namespace App\Http\Controllers;

use App\Author;
use GuzzleHttp\Client;
use http\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $messages = \App\Message::orderBy('posted_at', 'desc')->get();
        return view('home', [
            'messages' => $messages
        ]);
    }

    public function following_list()
    {
        $authors = Author::all();
        return view('following_list', [
            'authors' => $authors
        ]);
    }

    public function add_following(Request $request) {
        $author = new Author;
        $author_obj= json_decode(file_get_contents("http://127.0.0.1:5000/check?name=$request->twitter_id"), true);
        $author->display_name = $author_obj['name'];
        $author->name = $request->twitter_id;
        $author->description = $author_obj['description'];
        $author->save();
        return redirect()->route('following_list');
    }

    public function sync_following(Request $request) {
        $author = Author::find($request->author_id);
        $client = new Client;
        $ret = $client->get("http://127.0.0.1:5000/fetch_one?name=$author->name&author_id=$author->id");
        return redirect()->route('following_list');
    }

    public function delete_following(Request $request) {
        $author = Author::find($request->author_id);
        $author->delete();
        return redirect()->route('following_list');
    }
}
